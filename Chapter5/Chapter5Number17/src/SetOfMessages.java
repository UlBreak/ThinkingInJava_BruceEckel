//Брюс Эккель "Философия Java". Глава №5, Задание №17, Страница 177.
//Пишем программу в которой создаём класс с конструктором, получающий аргумент String. 
//Выведим значение аргумента во время конструирования. Создадим  массив сылок на этот класс, но не создаём объекты, которыми заполняется массив. 
//Запускаем программу и посмотрим, будут ли выводится сообщения при вызове конструктора.  

import java.util.Arrays;

class Messages {
    Messages(String[] Message) {
        System.out.println(Arrays.toString(Message));
    }
}

public class SetOfMessages {
    public static void main(String[] args) {
        String[] MyMessage = new String[5];

        new Messages(MyMessage);
    }
}

/*
Output:
[null, null, null, null, null]
 */
