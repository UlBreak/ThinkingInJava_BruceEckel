//Брюс Эккель "Философия Java". Глава №5, Задание №14, Страница 172. 
//Пишем программу, в которой создаём класс с полем static String, инициализируемые в точке определения, и другим полем, инициализируемым в блоке static. 
//Добавим статистческий метод, который выводит значения полей и демонстрирует, что обы поля инициализируются перед использованием.

class RickAndMorty {
    static String Speech;

    static {
        Speech = "Wubba Lubba ";

    }

    static void rickSays() {
        System.out.print(Speech);
        Speech = "Dub Dub";
        System.out.print(Speech);

    }


}

public class InitializationInFieldAndStaticBlock {
    public static void main(String[] args) {
        RickAndMorty Rick = new RickAndMorty();
        Rick.rickSays();
    }
}

/*
Output:
Wubba Lubba Dub Dub
 */
