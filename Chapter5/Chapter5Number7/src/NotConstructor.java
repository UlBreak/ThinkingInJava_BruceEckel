//Брюс Эккель "Философия Java". Глава №5, Задание №7, Страница 153. 
//Пишем программу в которой создаём класс без конструктора. Создайём объект этого класса в методе main(), чтобы удостовериться, 
//что контсруктор по умолчанию синтезируется автоматически.

class Constructor{}

public class NotConstructor {
    public static void main(String[] args) {
        Constructor NotConstructor = new Constructor();
        System.out.println(NotConstructor);
    }
}

/*
Output:
Constructor@5acf9800
 */
