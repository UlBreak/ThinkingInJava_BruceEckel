//Брюс Эккель "Философия Java". Глава №5, Задание №15, Страница 173.
//Пишем программу, в которой создаём класс, производный от String, инициализируемые в секции иниациализации экземпляров.

class Strings{
    Strings(String Text){
        System.out.println("\t " + Text);
    }
}
class DerivativeOfStrings{
    Strings DerivativeOfString;
    Strings DerivativeOfString1;
    {
        DerivativeOfString = new Strings("Производная от String №0.");
        DerivativeOfString1 = new Strings("Производная от String №1.");
        System.out.println("Обе производных от String инициализированы.");
    }
}
public class InitializationSection {
    public static void main(String[] args) {
        new DerivativeOfStrings();
    }
}

/*
Output:
	 Производная от String №0.
	 Производная от String №1.
Обе производных от String инициализированы.
 */
