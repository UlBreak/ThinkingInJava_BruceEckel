//Брюс Эккель "Философия Java". Глава №5, Задание №1, Страница 145. 
//Пишем програму, в которой создаём класс с инициализированной ссылкой на String. Покажем, что Java инициализирует ссылку значением null.

class uninitializedLink {
    String Uninitialized;

    uninitializedLink() {
        System.out.println(Uninitialized);
    }
}

public class UninitializedLink {
    public static void main(String[] args) {
        new uninitializedLink();
    }
}

/*
Output:
null
 */
