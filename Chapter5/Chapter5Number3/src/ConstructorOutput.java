//Брюс Эккель "Философия Java". Глава №5, Задание №3, Страница 152. 
//Пишем программу в которой, создаём класс к конструктором по умолчанию, который выводит на экран сообщние. Создадим объкт этого класса.

class EmptyConstructor{
    EmptyConstructor(){
        System.out.println("Пустой конструктор");
    }
}

public class ConstructorOutput {
    public static void main(String[] args) {
        EmptyConstructor EmCn = new EmptyConstructor();
    }
}

/*
Output:
Пустой конструктор
 */
