//Брюс Эккель "Философия Java". Глава №5, Задание №5, Страница 153. 
//Пишем программу в которой создадим класс Dog с перегруженным методом bark(). 
//Метод должен быть переружен для разных примитивных типов данных с целью вывода сообщения о лае, завывания,поскуливание и т.п. в зависимости от версии перегруженного метода. Все версии вызовем в main().

class Dog {
    void bark(int a) {
        System.out.println("Собака лает");
    }
    void bark(int a, double b){
        System.out.println("Собака воет");
    }
    void bark(int a, double b, float c){
        System.out.println("Собака скулит");
    }

}

public class BarkingDogs {
    public static void main(String[] args) {
        Dog Bulldog = new Dog();
        Bulldog.bark(1);
        Bulldog.bark(1,1.0);
        Bulldog.bark(1,1.0,1.0f);

    }
}

/*
Output:
Собака лает
Собака воет
Собака скулит
 */
