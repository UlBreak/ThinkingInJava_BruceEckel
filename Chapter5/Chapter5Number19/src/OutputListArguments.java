//Брюс Эккель "Философия Java". Глава №5, Задание №19, Страница 182. 
//Пишем программу в которой напишем метод, получающий список аргументов переменной длины с массивом String. 
//Убеждаемся в том, что этот метод может передоваться как список объектов String, разделёнными запятыми, так и String[].

public class OutputListArguments {
    static void outputListArguments(String... arg) {
        for (String S : arg) {
            System.out.print(S + " ");
        }
        System.out.println();
    }

    public static void main(String[] args) {
        outputListArguments("Список покупок:", "Говядина,", "Картошка,", "Соус.");
        String[] ShoppingList = {"Список покупок:", "Тетради,", "Ручки,", "Замазка."};
        outputListArguments(ShoppingList);
    }
}
/*
Output:
Список покупок: Говядина, Картошка, Соус.
Список покупок: Тетради, Ручки, Замазка.
*/
