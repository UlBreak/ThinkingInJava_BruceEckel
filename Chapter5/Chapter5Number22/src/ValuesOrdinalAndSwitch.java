//Брюс Эккель "Философия Java". Глава №5, Задание №22, Страница 184. 
//Пишем программу в которой пишем команду Switch для перечисления из предыдущего примера. 
//Для каждого случая выведим расщиренное описание каждой валюты.

enum Currency{
    USD,EUR,CHF,JPY,ZAR,CAD
}

public class ValuesOrdinalAndSwitch {
    Currency Details;
    public ValuesOrdinalAndSwitch(Currency Details){this.Details=Details;}
    public void describe(){
        System.out.print("Это валюта - ");
        switch (Details){
            case USD:
                System.out.println("Доллар, валюта которая обесценивается и её главенство окончится в скором будущем.");
                break;
            case EUR:
                System.out.println("Евро, она крепчает и растёт за ней будущее.");
                break;
            case CHF:
                System.out.println("Швейцарский франк, она из самых стабильных валют в мире.");
                break;
            case JPY:
                System.out.println("Японская иена, возможно имеет будущее.");
                break;
            case ZAR:
                System.out.println("Южноафриканский рэнд, я не что это, но это одна из самых популярных валют мира.");
                break;
            case CAD:
                System.out.println("Канадский доллар, также одна из самых популярных валют , есть и есть , чего бубнить.");
                break;

        }
    }

    public static void main(String[] args) {
        ValuesOrdinalAndSwitch
                USD= new ValuesOrdinalAndSwitch(Currency.USD),
                EUR= new ValuesOrdinalAndSwitch(Currency.EUR),
                ZAR= new ValuesOrdinalAndSwitch(Currency.ZAR);
        USD.describe();
        EUR.describe();
        ZAR.describe();
    }
}

/*
Output:
Это валюта - Доллар, валюта которая обесценивается и её главенство окончится в скором будущем.
Это валюта - Евро, она крепчает и растёт за ней будущее.
Это валюта - Южноафриканский рэнд, я не что это, но это одна из самых популярных валют мира.
 */
