//Брюс Эккель "Философия Java". Глава №5, Задание №10, Страница 161. 
//Пишем програму в котором, создаём  класс с методом finilaze(), который выводит сообщение. 
//В методе main() создаём объект нашего класса. 
//Объяснение повдения программы: класс finalize(),не будет работать если ему нечего проверять. 
//Брюс Эккель "Философия Java". Глава №5, Задание №10, Страница 161. Делаем так, чтобы метод finalize() обязательно был исполнен.

class Message {

    private boolean Checked = false;
    private String Message;

    Message(String MyMessage) {
        Message = MyMessage;
        check();
        finalize();
    }

    void check() {
        String DesiredMessage = "Hello, world!";
        if (DesiredMessage.equalsIgnoreCase(this.Message) == true) {
            Checked = true;
            System.out.println(Message);
        }
    }

    protected void finalize() {
        if (Checked == false) {
            System.out.println("Ошибка.");
        }
    }
}

public class MethodFinalize {
    public static void main(String[] args) {
        Message Msg = new Message("Hello, world!");
    }
}

