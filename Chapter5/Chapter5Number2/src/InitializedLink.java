//Брюс Эккель "Философия Java". Глава №5, Задание №2, Страница 145. 
//Напишем программу в которой создадим класс с полем String, инициализируемым в точке определения, и другим полем, инициализируемым конструктором.

class Initialized {
    String Initialized;

    Initialized() {
        this.Initialized = "Happiness";
        System.out.println(Initialized);
    }
}

public class InitializedLink {
    public static void main(String[] args) {
        new Initialized();
    }
}

/*
Output:
Happiness
 */


