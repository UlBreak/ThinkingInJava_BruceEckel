//Брюс Эккель "Философия Java". Глава №5, Задание №13, Страница 172. 
//Пишем программу в которой проверяем истинность из предыдущего абзаца.

class Cup{
    Cup(int marker){
        System.out.println("Cup("+marker+")");
    }
    void f(int marker){
        System.out.println("f("+marker+")");
    }
}
class Cups{
    static Cup cup1;
    static Cup cup2;
    static {
        cup1 = new Cup(1);
        cup2 = new Cup(2);
    }
    Cups(){
        System.out.println("Cups()");
    }
}

public class TestingTruthStatement {
    public static void main(String[] args) {
        //Cups.cup1.f(99); //(1)
    }
    static Cups Cups1 = new Cups(); //(2)
    static Cups Cups2 = new Cups(); //(2)
    static Cup Cup = new Cup(99); //(2)

}

/*
Output со строками (1):
Cup(1)
Cup(2)
f(99)

Output со строками (2):
Cup(1)
Cup(2)
Cups()
Cups()
Cup(99)
 */
