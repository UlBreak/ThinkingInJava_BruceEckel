//Брюс Эккель "Философия Java". Глава №5, Задание №4, Страница 152. 
//Пишем программу, в ChapterFiveNumberThree добавляем к классу перегруженный конструктор, 
//принимающий в качестве параметра String и распечатывающий её вместе с сообщением.
  
class Overloaded{
        Overloaded(){
            System.out.println("Пустой конструктор");
        }
        Overloaded(String Line){
            System.out.println(Line);
        }
    }

    public class OverloadedConstructor{
        public static void main(String[] args) {
            Overloaded EmCn = new Overloaded("Перегруженный конструктор");
        }
    }

/*
Output:
Перегруженный конструктор
 */
