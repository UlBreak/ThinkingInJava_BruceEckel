//Брюс Эккель "Философия Java". Глава №5, Задание №8, Страница 155. 
//Пишем программу в которой создаём класс с двумя методами. В первом методе дважды вызываем второй метод: один раз без ключчевого слова this, 
//а во втором с this - просто для того, чтобы убедиться в работоспособности этого синтаксиса. 

//!!!Не используёте этот способ вызова на практике!!!\\

class Raspberries{
    void cleaning(){
        System.out.println("Оторвать хвостики ягоды");
        this.shift();
        System.out.println("/  /");
        shift();
    }
    void shift(){
        System.out.println("Положить ягоду в тарелку");
    }
}

public class MethodWithThisAndWithoutThis {
    public static void main(String[] args) {
        Raspberries Strawberry = new Raspberries();
        Strawberry.cleaning();
    }
}

/*
Output:
Оторвать хвостики ягоды
Положить ягоду в тарелку
/  /
Положить ягоду в тарелку
 */

