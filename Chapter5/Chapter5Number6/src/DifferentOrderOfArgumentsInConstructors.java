//Брюс Эккель "Философия Java". Глава №5, Задание №6, Страница 153. 
//Пишем программу на основе репозитория ChapterFiveNumberFive, изменяем его так, 
//чтобы два перегруженных метода принимали два аргумента и отличались только порядком их следования в списке аргументов.

class Dog {
    void bark(int a) {
        System.out.println("Собака лает");
    }
    void bark(int a, double b){
        System.out.println("Собака воет");
    }
    void bark( double b, int a){
        System.out.println("Собака скулит");
    }

}

public class DifferentOrderOfArgumentsInConstructors {
    public static void main(String[] args) {
        Dog Bulldog = new Dog();
        Bulldog.bark(1);
        Bulldog.bark(1,1.0);
        Bulldog.bark(1.0,1);

    }
}

/*
Output:
Собака лает
Собака воет
Собака скулит
 */






