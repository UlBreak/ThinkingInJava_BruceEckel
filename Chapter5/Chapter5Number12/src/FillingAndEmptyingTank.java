//Брюс Эккель "Философия Java". Глава №5, Задание №12, Страница 161. 
//Пишем программу, в которой в которой создаём класс с именем Tank, который можно наполнить и опустошить. 
//Условие "готовности" требует, чтобы он был пуст перед очисткой. Напишим метод finalize(), проверяющий это условие. 
//В методе main() протеструем возможные случаи использвания нашего класса.

import java.util.Scanner;
import static Output.Output.*;

class Tank {
    int MaxVolume = 1000;
    int MinVolume = 0;
    private boolean CheckOut;

    void fillingUp(int FillingUp) {
        if ((MinVolume + FillingUp) <= MaxVolume) {
            MinVolume += FillingUp;
            println("В реверзуар налили " + FillingUp + " литров.");
            println("В реверзуаре " + MinVolume + " литров.");
        } else {
            MinVolume = 1000;
            println("Реверзуар полон.");
        }
    }

    void desolation(int Desolation) {
        if ((MinVolume - Desolation) >= 0) {
            MinVolume -= Desolation;
            println("Из реверзуара вылили " + Desolation + " литров.");
            println("В реверзуаре " + MinVolume + " литров.");
        } else {
            MinVolume = 0;
            println("Резервуар пуст.");

        }
    }

    void check() {
        boolean CheckOut;
        if (MinVolume == 0) {
            CheckOut = true;
        }
        else {
            CheckOut = false;
        }
        finalize();
    }

    public void finalize() {
        if (CheckOut == true) {
            println("Процес завершон.");
        }
        else {
            println("Ошибка, tank не пуст. Работа не может быть завершена.");
        }
    }
}

public class FillingAndEmptyingTank {
    public static void main(String[] args) {
        Scanner Scan = new Scanner(System.in);
        Tank Tanker = new Tank();
        println("Сколько нужно налить в Tanker ?");
        int FillingUp = Scan.nextInt();
        Tanker.fillingUp(FillingUp);
        System.out.println("Сколько нужно вылить из Tanker ?");
        int Desolation = Scan.nextInt();
        Tanker.desolation(Desolation);
        Tanker.check();

    }
}

/*
Output:
Сколько нужно налить в Tanker ?
666
В реверзуар налили 666 литров.
В реверзуаре 666 литров.
Сколько нужно вылить из Tanker ?
555
Из реверзуара вылили 555 литров.
В реверзуаре 111 литров.
Ошибка, tank не пуст. Работа не может быть завершена.

Output:

 */
