//Брюс Эккель "Философия Java". Глава №5, Задание №18, Страница 177. 
//Завершаем ChapterFiveNumberSeventeen - создаём объекты, которыми заполняем массив ссылок.

import java.util.Arrays;
import java.util.Scanner;

class Messages {
    Messages(String[] Message) {
        System.out.println(Arrays.toString(Message));
    }
}

public class SetOfMessages {
    public static void main(String[] args) {
        String[] MyMessage = new String[5];
        Scanner Scan = new Scanner(System.in);
        for (int i = 0; i < MyMessage.length; i++) {
            MyMessage[i] = Scan.nextLine();
        }
        new Messages(MyMessage);
    }
}

/*
Output:
[Привет., Как дела ?, Не занят ?, Нужна твоя помощь., Ты тут ?]
 */
