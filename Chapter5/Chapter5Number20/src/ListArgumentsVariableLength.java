//Брюс Эккель "Философия Java". Глава №5, Задание №20, Страница 182. 
//Пишем программу, в которой пишем метод main(), исользующий список аргументов переменной длины вместо обычного синтаксиса. 
//Выведим все элементы полученного массива args. Протестируем программу с разным количеством аргументов командной строки.

public class ListArgumentsVariableLength {
    static void OutputLAVL(Object... args){
        for (Object S: args)
            System.out.print(S + " ");
        System.out.println();
    }

    public static void main(String[] args) {
        OutputLAVL("SublimeText");
        OutputLAVL("SublimeText",5.65);
        OutputLAVL("SublimeText",5.65,6.5f);
        OutputLAVL("SublimeText",5.65,6.5f,5.55d);
        OutputLAVL("SublimeText",5.65,6.5f,5.55d,true);
    }
}

/*
Output:
SublimeText
SublimeText 5.65
SublimeText 5.65 6.5
SublimeText 5.65 6.5 5.55
SublimeText 5.65 6.5 5.55 true
 */
