//Брюс Эккель "Философия Java". Глава №5, Задание №16, Страница 177. 
//Пишем программу, в которой создаём массив объктов String. 
//Присваиваем объект String каждому элементу массива. Выведем содержимое масива в цикле for.

public class FavoriteCars {
    public static void main(String[] args) {
        String[] Cars={"Audi A6 Avant","Peugeot 508 Universal","Renault Megane II Universal","Volkswagen Alltrack","Subaru Levorg","Volvo V90"};
        for (int i=0;i<Cars.length;i++){
            System.out.println(Cars[i]);
        }
    }
}

/*
Output:
Audi A6 Avant
Peugeot 508 Universal
Renault Megane II Universal
Volkswagen Alltrack
Subaru Levorg
Volvo V90
 */
