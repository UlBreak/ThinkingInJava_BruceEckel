//Брюс Эккель "Философия Java". Глава №5, Задание №9, Страница 156. 
//Пишем программу в которой, подготовим два класса, с двумя перегруженными конструкторами. Используя ключевое слово this, вызовим второй конструктр из первого.

import java.util.Scanner;

class Airplane {

    Airplane(String CitySent, String ArrivalCity, double TravelTime) {
        this("A319", 5, 20.30);
        System.out.println("Отправления из города: " + CitySent + ".");
        System.out.println("Прибытие в город: " + ArrivalCity + ".");
        System.out.println("Время полёта " + TravelTime + " часов.");
    }

    Airplane(String Series, int Strip, double TakeOffTime) {
        System.out.println("Самолёт " + Series + ".");
        System.out.println("На " + Strip + " полосе.");
        System.out.println("Взлетает в " + TakeOffTime + ".");
    }
}

public class TwoConstructorsAndThis {
    public static void main(String[] args) {
        Airplane Passenger = new Airplane("Москва", "Краснодар", 5.5);
    }
}

/*
Output:
Самолёт A319.
На 5 полосе.
Взлетает в 20.3.
Отправления из города: Москва.
Прибытие в город: Краснодар.
Время полёта 5.5 часов.
 */
