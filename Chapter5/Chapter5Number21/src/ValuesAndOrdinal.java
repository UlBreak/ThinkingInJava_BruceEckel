//Брюс Эккель "Философия Java". Глава №5, Задание №21, Страница 184. 
//Пишем программу в которой, создаём перечисление с названиями шести типов бумажных денег. 
//Переберём результат values() c выводом каждого значние и его ordinal().

enum Currency{
    USD,EUR,CHF,JPY,ZAR,CAD
}

public class ValuesAndOrdinal {
    public static void main(String[] args) {
        for (Currency C: Currency.values())
            System.out.println(C+", Валюта № "+ C.ordinal());
    }
}

/*
Output:
USD, Валюта № 0
EUR, Валюта № 1
CHF, Валюта № 2
JPY, Валюта № 3
ZAR, Валюта № 4
CAD, Валюта № 5
 */
