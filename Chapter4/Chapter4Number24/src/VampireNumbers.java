//Брюс Эккель "Философия Java". Глава №4, Задание №24, Страница 142. Пишем программу , для поиска всех четырёх значных чисел "Вампиров".

public class VampireNumbers {
    public static void main(String[] args) {
        int Term1, Term2, VampireNumber = 0, k;
        for (int i = 10; i <= 99; i++) {
            Term1 = i;
            k = 0;

            for (int j = 10; j <= 99; j++) {
                Term2 = j;
                VampireNumber = Term1 * Term2;
                k = 0;
                int Examination = VampireNumber;
                while (Examination > 0) {  //для проверки количества разрядов в числе
                    Examination = Examination / 10;
                    k++;
                }

                int NumberRank1 = VampireNumber / 1000, NumberRank2 = VampireNumber / 100 % 10, NumberRank3 = VampireNumber / 10 % 10, NumberRank4 = VampireNumber % 10;

                if (k == 4) {

                    if ((NumberRank1 * 10 + NumberRank2) * (NumberRank3 * 10 + NumberRank4) == VampireNumber) {
                        System.out.println(VampireNumber + " = " + (NumberRank1 * 10 + NumberRank2) + "*" + (NumberRank3 * 10 + NumberRank4));
                        break;
                    }
                    if ((NumberRank2 * 10 + NumberRank1) * (NumberRank4 * 10 + NumberRank3) == VampireNumber) {
                        System.out.println(VampireNumber + " = " + (NumberRank2 * 10 + NumberRank1) + "*" + (NumberRank4 * 10 + NumberRank3));
                        break;
                    }
                    if ((NumberRank1 * 10 + NumberRank4) * (NumberRank2 * 10 + NumberRank3) == VampireNumber) {
                        System.out.println(VampireNumber + " = " + (NumberRank1 * 10 + NumberRank4) + "*" + (NumberRank2 * 10 + NumberRank3));
                        break;
                    }
                    if ((NumberRank4 * 10 + NumberRank1) * (NumberRank3 * 10 + NumberRank2) == VampireNumber) {
                        System.out.println(VampireNumber + " = " + (NumberRank4 * 10 + NumberRank1) + "*" + (NumberRank3 * 10 + NumberRank2));
                        break;
                    }
                    if ((NumberRank1 * 10 + NumberRank3) * (NumberRank2 * 10 + NumberRank4) == VampireNumber) {
                        System.out.println(VampireNumber + " = " + (NumberRank1 * 10 + NumberRank3) + "*" + (NumberRank2 * 10 + NumberRank4));
                        break;
                    }
                    if ((NumberRank3 * 10 + NumberRank1) * (NumberRank2 * 10 + NumberRank2) == VampireNumber) {
                        System.out.println(VampireNumber + " = " + (NumberRank3 * 10 + NumberRank1) + "*" + (NumberRank2 * 10 + NumberRank2));
                        break;
                    }
                    if ((NumberRank1 * 10 + NumberRank2) * (NumberRank4 * 10 + NumberRank3) == VampireNumber) {
                        System.out.println(VampireNumber + " = " + (NumberRank1 * 10 + NumberRank2) + "*" + (NumberRank4 * 10 + NumberRank3));
                        break;
                    }
                    if ((NumberRank2 * 10 + NumberRank1) * (NumberRank3 * 10 + NumberRank4) == VampireNumber) {
                        System.out.println(VampireNumber + " = " + (NumberRank2 * 10 + NumberRank1) + "*" + (NumberRank3 * 10 + NumberRank4));
                        break;
                    }
                    if ((NumberRank2 * 10 + NumberRank3) * (NumberRank4 * 10 + NumberRank1) == VampireNumber) {
                        System.out.println(VampireNumber + " = " + (NumberRank2 * 10 + NumberRank3) + "*" + (NumberRank4 * 10 + NumberRank1));
                        break;
                    }
                    if ((NumberRank3 * 10 + NumberRank2) * (NumberRank1 * 10 + NumberRank4) == VampireNumber) {
                        System.out.println(VampireNumber + " = " + (NumberRank3 * 10 + NumberRank2) + "*" + (NumberRank1 * 10 + NumberRank4));
                        break;
                    }
                    if ((NumberRank1 * 10 + NumberRank3) * (NumberRank4 * 10 + NumberRank2) == VampireNumber) {
                        System.out.println(VampireNumber + " = " + (NumberRank1 * 10 + NumberRank3) + "*" + (NumberRank4 * 10 + NumberRank2));
                        break;
                    }
                    if ((NumberRank3 * 10 + NumberRank1) * (NumberRank2 * 10 + NumberRank4) == VampireNumber) {
                        System.out.println(VampireNumber + " = " + (NumberRank3 * 10 + NumberRank1) + "*" + (NumberRank2 * 10 + NumberRank4));
                        break;
                    }

                }
            }
        }
    }
}

/*
Output:
Нужно изменять программу, она не эфективна.
1260 = 21*60
1260 = 21*60
1530 = 51*30
1260 = 21*60
1260 = 21*60
1260 = 21*60
2376 = 72*33
2187 = 81*27
1260 = 21*60
1827 = 21*87
1260 = 21*60
1395 = 93*15
2376 = 72*33
1530 = 51*30
1260 = 21*60
1260 = 21*60
1435 = 41*35
1260 = 21*60
2376 = 72*33
1260 = 21*60
1530 = 51*30
2376 = 72*33
1260 = 21*60
1260 = 21*60
2376 = 72*33
1260 = 21*60
2376 = 72*33
5775 = 75*77
5775 = 75*77
6880 = 86*80
2187 = 81*27
1260 = 21*60
1530 = 51*30
6880 = 86*80
1827 = 21*87
2376 = 72*33
1260 = 21*60
1395 = 93*15
2376 = 72*33
 */
