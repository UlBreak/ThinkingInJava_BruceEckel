//№4, Задание №19. Страница 131. 
//Пишем программу которая, используя тернарный оператор и поразрядную проверку для вывоа нулей и идениц (вместо вызова метода Intenger.tiBinaryString()).

import java.util.Scanner;
import static Cout.Println.println;
import static Cout.Print.print;


public class IntoBinary {
    public static void main(String[] args) {
        Scanner Scan = new Scanner(System.in);
        int Number = Scan.nextInt();
        println("Введённое число: "+ Number);
        println();
        print("Двоичное представление этого числа: ");
        if (Number > 1) {
            while (Number > 0) {
                print(Number % 2);
                Number = Number / 2;
            }
        }
    }
}

/*
Output:
Введённое число: 8
Двоичное представление этого числа: 0001 число необходимо читать справа на лево
 */
