//Брюс Эккель "Философия Java". Глава №4, Задание №22, Страница 141. 
//Пишем программу, в которой создаём команду switch, которая выводит сообщение в каждой секции case. 
//Размешаем его в цикле for, проверяющий все допустимые зачения case. Каждая секция case должна завершаться командой break.
//Можете удалить команды break и посмотреть что будет... 

import static Output.Output.*;

public class SwitchInFor {
    public static void main(String[] args){
        for (int DayOfWeek=1;DayOfWeek<=8;DayOfWeek++){
            switch (DayOfWeek){
                case 1 : println("Понедельник"); break;
                case 2 : println("Вторник"); break;
                case 3 : println("Среда"); break;
                case 4 : println("Четверг"); break;
                case 5 : println("Пятница"); break;
                case 6 : println("Суббота"); break;
                case 7 : println("Воскресенье"); break;
                default: println("Вообщет в неделе 7 дней...");
            }
        }
    }
}

/*
Output:
Понедельник
Вторник
Среда
Четверг
Пятница
Суббота
Воскресенье
Вообщет в неделе 7 дней...
 */
