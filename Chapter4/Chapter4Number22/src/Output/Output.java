package Output;

public class Output {
    public static void print(Object...args){
        for (Object O : args){
            System.out.print(O.toString());
        }
    }
    public static void println(Object...args){
        for (Object O : args){
            System.out.println(O.toString());
        }
    }
}
