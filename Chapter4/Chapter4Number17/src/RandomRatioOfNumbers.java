//Брюс Эккель "Философия Java". Глава №4, Задание №17. Страница 130. 
//Изменяем ChapterFourNumberSixteen так, чтобы код выполнялся в "бесконечном" цикле while. 
//Программа должна работать до тех пор, пока её выполнение не будет прерванно с клавиатуры. 

import java.util.Random;

public class RandomRatioOfNumbers {
    public static void main(String[] args) {
        Random Rnd = new Random();
        while (true) {
            int Number = Rnd.nextInt(30);
            int Number1 = Rnd.nextInt(30);
            System.out.println("\t > Пара чисел");
            System.out.println(Number + " - первое число");
            System.out.println(Number1 + " - второе число");
            if (Number < Number1) {
                System.out.println("Второе число больше первого");

            }
            if (Number > Number1) {
                System.out.println("Второе число меньше первого");

            }
            if (Number == Number1) {
                System.out.println("Числа равны");

            }
        }
    }
}

/*
Output:
 > Пара чисел
29 - первое число
28 - второе число
Второе число меньше первого
	 > Пара чисел
7 - первое число
14 - второе число
Второе число больше первого
	 > Пара чисел
9 - первое число
4 - второе число

В IDEA программа прерывается с клавиатуры, сочетанием Ctrl + F2.
 */
