//Брюс Эккель "Философия Java". Глава №4, Задание №15, Страница 130. Пишем программу, которая выводит числа от 1 до 100 через for.

public class FromZeroToOneHundred {
    public static void main(String[] args){
        for (int Number = 1;Number<=100;Number++){
            System.out.println(Number);
        }
    }
}

/*
Output:
1
2
3
...
98
99
100
*/
