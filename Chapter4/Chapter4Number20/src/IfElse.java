//Брюс Эккель "Философия Java". Глава №4, Задание №20, Страница 134. 
//Пишем программу, в которой изменяем метод test() так, чтобы он получал два дополнительных аргумента degin и end, 
//а значение testVal проверялось на принадлежности к диапозону [begin,end].

public class IfElse {
    static int test(int TestVal, int Target, int Begin, int End){
        if ((TestVal > Target)&&((TestVal>=Begin)&&(TestVal<=End))){
            return +1  ;
        }
        if ((TestVal < Target)&&((TestVal>=Begin)&&(TestVal<=End))){
            return -1;
        }
        else {
            return 0;
        }
    }
    public static void main(String[] args){
        System.out.println(test(10,5,7,14));
        System.out.println(test(7,10,7,14));
        System.out.println(test(9,9,7,14));
    }
}

/*
Output:
 1
-1
 0
 */


