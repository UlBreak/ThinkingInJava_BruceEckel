//Брюс Эккель "Философия Java". Глава №4, Задание №23, Страница 142. 
//Пишем программу, содержащую метод, который получает чилочисленный аргумент и выводит указаное количество чисел Фибоначчи.

import java.util.Scanner;
import static Output.Output.*;

public class FibonacciNumbers {
    static void Fibonacci(int Amount) {
        if (Amount > 2) {
            int Number1 = 1;
            int Number2 = 1;
            int SumNumber;
            print(Number1 + " " + Number2 + " ");
            for (int j = 2; j < Amount; j++) {
                SumNumber = Number1 + Number2;
                print(SumNumber + " ");
                Number1 = Number2;
                Number2 = SumNumber;


            }
        }
        if (Amount == 2)
            print(1 + " " + 1);

        if (Amount == 1)
            print(1);

        if (Amount == 0)
            print("Пустота");
    }


    public static void main(String[] args) {
        println("Какое количество чисел Фибоначчи вы хотите вывести ?");
        Scanner Scan = new Scanner(System.in);
        int AmountNumber = Scan.nextInt();
        Fibonacci(AmountNumber);
    }
}

/*
Output:
Какое количество чисел Фибоначчи вы хотите вывести ?
12
1 1 2 3 5 8 13 21 34 55 89 144 
 */
