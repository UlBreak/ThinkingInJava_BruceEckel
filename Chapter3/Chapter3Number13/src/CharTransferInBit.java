//Брюс Эккель "Философия Java". Глава №3, Задание №13, Страница 113.
//Пишем программу содержащую метод для вывода char в двоичном представление. 
//И демонстрируем его работу на нескольких примерах.

public class CharTransferInBit {
    private static void charInBit(char Variable) {
        System.out.println("Значение char Variable0: " + Variable + ". Значение char Variable0 в 2 сс: " + Long.toBinaryString(Variable));
    }

    public static void main(String[] args) {
        charInBit((char) 32);
        charInBit((char) 536);
        charInBit((char) 77);
    }
}
/*
Output:
Значение char Variable0:  . Значение char Variable0 в 2 сс: 100000
Значение char Variable0: Ș. Значение char Variable0 в 2 сс: 1000011000
Значение char Variable0: M. Значение char Variable0 в 2 сс: 10110
 */
