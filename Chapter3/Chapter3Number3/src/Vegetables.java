import static Print.Print.print;

//Брюс Эккель "Философия Java". Глава №3, Задание №3, Страница 99. 
//Пишем программу с классом содержаший поле типа float. Используем этот класс для демонстрации совмещения имён при вызове методов.
class Weight {
    float Cucumbers;
}

public class Vegetables {

    static void Method(Weight Cuc) {
        Cuc.Cucumbers = 5.3f;
    }

    public static void main(String[] args) {
            Weight Wgt = new Weight();
            Wgt.Cucumbers = 3.5f;
            print(Wgt.Cucumbers + " кг огурцов");
            Method(Wgt);
            print(Wgt.Cucumbers + "кг огурцов");
    }
}

/*
Output:
3.5 кг огурцов
5.3кг огурцов
 */
