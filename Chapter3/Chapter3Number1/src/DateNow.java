import java.util.Date;
import static Print.Print.*; //или так import static Print.Print.print;


//Брюс Эккель "Философия Java". Глава №3, Задание №1, Страница 96. 
//Пишем программу в которой используются длинные и короткие команды печати.
public class DateNow {
    public static void main(String[] args) {
        System.out.print("Now: ");
        print(new Date());
    }
}

/*
Output:
Now: Wed Aug 12 21:22:12 MSK 2021
*/
