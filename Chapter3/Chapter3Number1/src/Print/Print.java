package Print;

public class Print {
    public static void print(Object... args) {  //метод для краткой печати
        for (Object o : args) {
            System.out.print(o.toString());
        }
    }
}
