import static Print.Print.print;
class Weight {
    float Weight;
}

//Брюс Эккель "Философия Java". Глава №3, Задание №2, Страница 98. 
//Пишем программу с классом с полем float. И используем его для демонстрации совмещения имён.
public class Products {
    public static void main(String[] args) {

        Weight ChickenBreast = new Weight();
        Weight BeefCuts = new Weight();

        ChickenBreast.Weight = 3.5f;
        BeefCuts.Weight = 2.0f;
        print("Вес куриной грудки : " + ChickenBreast.Weight + " кг Вес говяжьей нарезки : " + BeefCuts.Weight + " кг");

        ChickenBreast.Weight = BeefCuts.Weight;
        BeefCuts.Weight=4.0f;
        print("Вес куриной грудки : " + ChickenBreast.Weight + " кг Вес говяжьей нарезки : " + BeefCuts.Weight + " кг");

        ChickenBreast = BeefCuts;
        BeefCuts.Weight = 1.0f;
        print("Вес куриной грудки : " + ChickenBreast.Weight + " кг Вес говяжьей нарезки : " + BeefCuts.Weight + " кг");
    }
}

/*
Output
Вес куриной грудки : 3.5 кг Вес говяжьей нарезки : 2.0 кг
Вес куриной грудки : 2.0 кг Вес говяжьей нарезки : 4.0 кг
Вес куриной грудки : 1.0 кг Вес говяжьей нарезки : 1.0 кг
 */


