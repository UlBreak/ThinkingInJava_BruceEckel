//Брюс Эккель "Философия Java". Глава №3, Задание №12, Страница 113. 
//Пишем программу в которой начинаем с числа, состоящего из двоичных единиц. 
//Сдвигаем его влево, а затем используем беззнаковый оператор сдвига вправо по всем двоичным позициями, с выводом всех промежуточных результатов методом Integer.toBinaryString().

public class BitOffset {
    public static void main(String[] args) {
        int Variable = 0b1111111;
        System.out.println("\t> Начальное значение");
        System.out.println(Integer.toBinaryString(Variable));
        System.out.println("\t> Промежуточные значения");
        Variable <<= 1;
        System.out.println(Integer.toBinaryString(Variable));
        for (int i = 1; i < 7; i++) {
            Variable >>>= 1;
            System.out.println(Integer.toBinaryString(Variable));
        }
        System.out.println("\t> Конечное значение");
        System.out.println(Variable >>> 1);

    }
}

/*
Output:
	> Начальное значение
1111111
	> Промежуточные значения
11111110
1111111
111111
11111
1111
111
11
	> Конечное значение
1
 */
