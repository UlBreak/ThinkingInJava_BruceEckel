//Брюс Эккель "Философия Java". Глава №3, Задание №14, Страница 126. 
//Пишем программу ,содержащую метод который получает два аргумента String и выполняет с ними все операции логических сравнений и выводит результаты. 
//Для операции == и != также выполните проверку equals(). Вызываем метод из main() для нескольких разных объектов String.

import java.util.Scanner;
import static Println.Println.println;

public class ComparisonOfTwoString {
    public static void comparisonStrings(String Variable1, String Variable2) {
        println("\t> V1 == V2");
        println(Variable1 == Variable2);
        println("\t> V1 != V2");
        println(Variable1 != Variable2);
        println("\t> V1.equalsIgnoreCase.V2");
        println(Variable1.equalsIgnoreCase(Variable2));
        println("\t> V1.equals.V2 ");
        println(Variable1.equals(Variable2));
        println("\t> V1.equals.V2 == V2.equals.V1");
        println(Variable1.equals(Variable2) == Variable2.equals(Variable1));
        println("\t> V1.equals.V2 != V2.equals.V1");
        println(Variable1.equals(Variable2) != Variable2.equals(Variable1));
    }
    public static void main(String[] args) {
        Scanner Scan = new Scanner(System.in);
        String Variable1, Variable2;
        Variable1 = Scan.nextLine();
        Variable2 = Scan.nextLine();
        comparisonStrings(Variable1, Variable2);
    }
}
/*
Output:
Dog
Cat
	> V1 == V2
false
	> V1 != V2
true
	> V1.equalsIgnoreCase.V2
false
	> V1.equals.V2
false
	> V1.equals.V2 == V2.equals.V1
true
	> V1.equals.V2 != V2.equals.V1
false

Output:
Java
java
	> V1 == V2
false
	> V1 != V2
true
	> V1.equalsIgnoreCase.V2
true
	> V1.equals.V2
false
	> V1.equals.V2 == V2.equals.V1
true
	> V1.equals.V2 != V2.equals.V1
false

Output:
IDEA
IDEA
	> V1 == V2
false
	> V1 != V2
true
	> V1.equalsIgnoreCase.V2
true
	> V1.equals.V2
true
	> V1.equals.V2 == V2.equals.V1
true
	> V1.equals.V2 != V2.equals.V1
false
 */
