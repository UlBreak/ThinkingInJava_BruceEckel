//Брюс Эккель "Философия Java". Глава №3, Задание №7, Страница 105. 
//Пишем програму моделирующую бросок монетки.

import static Print.PrintVariations.*;

import java.util.Random;

public class CoinToss {
    public static void coinToss() {
        Random Coin = new Random();
        int SideCoin = Coin.nextInt(3);

        if (SideCoin == 1) {
            println("Выпал Орёл");
        }
        if (SideCoin == 2) {
            println("Выпала Решка");
        }
        if (SideCoin == 0) {
            println("Монетка упала не корректно, перекиньте монетку");
        }
    }

    public static void main(String[] args) {
        for (int i = 0; i < 3; i++) {
            coinToss();
        }
    }

}

/*
Output:
Выпала Решка
Монетка упала не корректно, перекиньте монетку
Выпал Орёл
 */
