package Print;

public class PrintVariations {
    static public void print(Object... args) {
        for (Object o : args) {
            System.out.print(o.toString());
        }
    }

    static public void println(Object... args) {
        for (Object o : args) {
            System.out.println(o.toString());
        }
    }
}
