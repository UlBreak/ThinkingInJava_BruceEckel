//Брюс Эккель "Философия Java". Глава №3, Задание №8, Страница 108. 
//Пишем программу, чтобы показать, что шестнадцатеричная и восьмеричная запись может использоваться с типом long.
//Для вывода результатов использовать метод long.toBinaryString().

import static java.lang.Long.toBinaryString;

public class WorkWithLong {
    public static void main(String[] args) {
        long HexadecimalVariable = 0X75AD; //шестнадцатеричная запись начинается с "0x || 0X", а далее числа от 0 до 9 и буквы от A до F
        long OctalVariable = 0756; //восьмеричная запись начинается с "0", а далее число от 0 до 7

        System.out.println("Шестнадцатеричная запись числа " + HexadecimalVariable + " при переводе в двоичную сс равна : " + toBinaryString(HexadecimalVariable));
        System.out.println("Шестнадцатеричная запись числа " + OctalVariable + " при переводе в двоичную сс равна : " + toBinaryString(OctalVariable));

    }
}
/*
Output:
Шестнадцатеричная запись числа 30125 при переводе в двоичную сс равна : 111010110101101
Восмеричная запись числа 494 при переводе в двоичную сс равна : 111101110
*/
