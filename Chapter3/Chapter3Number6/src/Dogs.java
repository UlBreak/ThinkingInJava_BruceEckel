//Брюс Эккель "Философия Java". Глава №3, Задание №6, Страница 104. 
//В репозитории ChapterThreeNumberFive создаем новую ссылку на Dog и присваиваем её объекту spot. 
//Сравниваем ссылку оператором == и методом equals().

import java.sql.SQLOutput;

class Dog {
    String name;
    String says;
}

public class Dogs {


    public static void main(String[] args) {
        Dog Rottweiler = new Dog();
        Rottweiler.name = "Spot";
        Rottweiler.says = "Growl-growl";

        Dog Pug = new Dog();
        Pug.name = "Scruffy";
        Pug.says = "Woof-woof";

        System.out.println("Ротвейлера зовут " + Rottweiler.name);
        System.out.println("Он рычит: " + Rottweiler.says);
        System.out.println();
        System.out.println("Мопса зовут " + Pug.name);
        System.out.println("Мопс тяфкает: " + Pug.says);

        Dog Dachshund = new Dog();
        Dachshund.name="Saymon";
        Dachshund.says="Gav-Gav";
        Dachshund = Pug;

        System.out.println();
        System.out.println(Dachshund == Pug);
        System.out.println(Dachshund.equals(Pug));

    }
}

/*
Output:
Ротвейлера зовут Spot
Он рычит: Growl-growl

Мопса зовут Scruffy
Мопс тяфкает: Woof-woof

true
true
 */
