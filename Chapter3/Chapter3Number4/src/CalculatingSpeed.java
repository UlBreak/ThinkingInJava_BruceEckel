import java.util.Scanner;
import static Print.PrintVariations.*;

//Брюс Эккель "Философия Java". Глава №3, Задание №4, Страница 101. 
//Пишем программу которая вычисляет скорость при известных значения расстояние и времени.

public class CalculatingSpeed {
    public static void main (String[] args) {
        Scanner Scan = new Scanner(System.in);
        println("Давайте найдем среднюю скорость");
        println("Сколько км проехал транспорт ?");
        float S = Scan.nextFloat();
        println("Сколько времени (в часах) транспорт находился в пути ?");
        float t= Scan.nextFloat();
        float V = S/t;
        print("Скорость транспорта равна: " + V +" км/ч");
    }
}
/*
Output:
Давайте найдем среднюю скорость
Сколько км проехал транспорт ?
100
Сколько времени (в часах) транспорт находился в пути ?
2,5
Скорость транспорта равна: 40.0 км/ч
 */
