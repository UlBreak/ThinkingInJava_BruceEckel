//Брюс Эккель "Философия Java". Глава №3, Задание №10, Страница 110. 
//Пишем программу с двумя константами : обе константы состоят из чередующихся нулей и единиц , но у одной нулю равен младший бит, а другой старший. 
//Объединяем эти две константы всеми возможными поразрядными операторами.

public class BitwiseOperators {
    public static void main(String[] args) {
        final int VariableOne = 0b101010;
        final int VariableTwo = 0b010101;

        System.out.println("\t>" + "Инверсия (NOT)");
        System.out.println(Integer.toBinaryString(~VariableOne));
        System.out.println(Integer.toBinaryString(~VariableTwo));

        System.out.println("\t>" + "Побитовое И (AND)");
        System.out.println(Integer.toBinaryString(VariableOne & VariableTwo));

        System.out.println("\t>" + "Побитовое ИЛИ (OR)");
        System.out.println(Integer.toBinaryString(VariableOne | VariableTwo));

        System.out.println("\t>" + "Исключающие ИЛИ (XOR)");
        System.out.println(Integer.toBinaryString(VariableOne ^ VariableTwo));

        System.out.println("\t>" + "Побитовый сдвиг вправо");
        System.out.println(Integer.toBinaryString(VariableOne >> VariableTwo));

        System.out.println("\t>" + "Побитовый сдвиг влево");
        System.out.println(Integer.toBinaryString(VariableOne >> VariableTwo));

        System.out.println("\t>" + "Побитовый сдвиг вправо с заполнением нулями");
        System.out.println(Integer.toBinaryString(VariableOne >>> VariableTwo));
    }
}

/*
Output:
    >Инверсия (NOT)
11111111111111111111111111010101
11111111111111111111111111101010
	>Побитовое И (AND)
0
	>Побитовое ИЛИ (OR)
111111
	>Исключающие ИЛИ (XOR)
111111
	>Побитовый сдвиг вправо
0
	>Побитовый сдвиг влево
0
	>Побитовый сдвиг вправо с заполнением нулями
0
 */
