//Брюс Эккель "Философия Java". Глава №3, Задание №5, Страница 104. 
//Создаём класс Dog, содержаший два поля типа String: name и says. В методе main() создаём два объекта с разными именами (spot и scruffy) и сообщениями. 
//Выводим значение обоих полей для каждого из объектов.

class Dog {
    String name;
    String says;
}

public class Dogs {


    public static void main(String[] args) {
        Dog Rottweiler = new Dog();
        Rottweiler.name = "Spot";
        Rottweiler.says = "Growl-growl";

        Dog Pug = new Dog();
        Pug.name = "Scruffy";
        Pug.says = "Woof-woof";

        System.out.println("Ротвейлера зовут " + Rottweiler.name);
        System.out.println("Он рычит: " + Rottweiler.says);
        System.out.println();
        System.out.println("Мопса зовут " + Pug.name);
        System.out.println("Мопс тяфкает: " + Pug.says);


    }
}

/*
Output:
Ротвейлера зовут Spot
Он рычит: Growl-growl

Мопса зовут Scruffy
Мопс тяфкает: Woof-woof
 */
