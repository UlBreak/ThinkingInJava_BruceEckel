//Брюс Эккель "Философия Java". Глава №3, Задание №11, Странциа 113. 
//Пишем программу в которой начинаем с двоичного числа, содержащее двоичную 1 в старшем бите. 
//Используем знаковый оператор сдвига вправо, сдвигаем знак до крайней правой позиции, с выводом всех промежуточных результатом методом Integer.toBinaryString().

public class BitOffset {
    public static void main(String[] args) {
        int Variable = 0b101010;
        System.out.println("\t> Начальное значение");
        System.out.println(Long.toBinaryString(Variable));
        System.out.println("\t> Промежуточные значения сдвига");
        for (int i = 1; i < 5; i++) {
            Variable >>= 1;
            System.out.println(Integer.toBinaryString(Variable));
        }
        System.out.println("\t> Конечное значение");
        System.out.println(Integer.toBinaryString(Variable>>1));
    }
}

/*
Output:
	> Начальное значение
101010
	> Промежуточные значения сдвига
10101
1010
101
10
	> Конечное значение
1
 */
