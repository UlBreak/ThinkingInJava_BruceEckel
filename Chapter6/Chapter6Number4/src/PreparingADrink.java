import hotDrink.*;
public class PreparingADrink extends HotDrink{
    public static void main(String[] args) {
        PreparingADrink Tea = new PreparingADrink();
        Tea.beverageName("Ahmad Tea London");
    }
}

/*
Output:
Берём Ahmad Tea London.
Заливаем Ahmad Tea London кипятком.
Ждём пять минут и можем пить.
 */