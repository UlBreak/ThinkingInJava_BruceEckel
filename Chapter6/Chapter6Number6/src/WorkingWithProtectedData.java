import java.util.Scanner;

class TabletSpecifications {
    protected String model;
    protected String colour;
    protected float screen;
    protected int cpu;
}

public class WorkingWithProtectedData {
    public static void main(String[] args) {
        Scanner Scan = new Scanner(System.in);
        TabletSpecifications iPad = new TabletSpecifications();
        System.out.println("Введите модель планшета.");
        iPad.model=Scan.nextLine();
        System.out.println("Введите цвет планшета.");
        iPad.colour=Scan.nextLine();
        System.out.println("Введите диагональ планшета.");
        iPad.screen=Scan.nextFloat();
        System.out.println("Введите процесор планшета.");
        iPad.cpu=Scan.nextInt();
        System.out.println();
        System.out.println("Модель планшета: "+iPad.model);
        System.out.println("Цвет планшета: " + iPad.colour);
        System.out.println("Диагональ планшета: " + iPad.screen);
        System.out.println("Модель процесора: A" + iPad.cpu + " Bionic");
    }
}

/*
Output:
Модель планшета: iPad Air
Цвет планшета: Space Gray
Диагональ планшета: 10.9
Модель процесора: A14 Bionic
 */