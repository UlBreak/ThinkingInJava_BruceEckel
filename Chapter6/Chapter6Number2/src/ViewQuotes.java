import interestingQuotes.*;
import quotes.*;

public class ViewQuotes {
    public static void main(String[] args) {
        Quotes Qts = new Quotes("Дай человеку пистолет, и он сможет ограбить банк. Но дай ему банк, и он сможет ограбить мир.");
    }
}

/*
Output:
java: reference to Quotes is ambiguous
  both class Quotes.Quotes in Quotes and class InterestingQuotes.Quotes in InterestingQuotes match
 */
