class AppleComputerInc {
    public static void mobileTechnics() {
        System.out.println("Мобильная техника: iPhone,iPad.");
    }

    protected void expensiveTechnics() {
        System.out.println("Дорогостоящая техника: iMac, MacBook, Mac Pro, Mac Mini.");
    }

    private void specialTechnics() {
        System.out.println("Спецефичная техника: iPod, Apple Watch, Air Pods, Apple TV");
    }
    public double Price_iPhone;
    protected double Price_iPad;
    private double PriceAirPods;

}

public class HoldingCompany {
    public static void main(String[] args) {
        AppleComputerInc Apple = new AppleComputerInc();
        Apple.mobileTechnics();
        Apple.expensiveTechnics();
        /* Apple.specialTechnics();
        Output:
        java: specialTechnics() has private access in AppleComputerInc
        */
        Apple.Price_iPhone = 699.0;
        System.out.println("Цена iPhone: " +  Apple.Price_iPhone + " $.");
        Apple.Price_iPad = 499.0;
        System.out.println("Цена iPad: " + Apple.Price_iPad + " $.");
        /* Apple.Air_Pods = 40.00;
        Output:
        java: Air_Pods has private access in AppleComputerInc
        */
    }
}

/*
Output:
Мобильная техника: iPhone,iPad.
Дорогостоящая техника: iMac, MacBook, Mac Pro, Mac Mini.
Цена iPhone: 699.0 $.
Цена iPad: 499.0 $.
 */