package access;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Widget {
    public static class DateTimeWidget {
        public DateTimeWidget() {
            LocalDateTime dateTimeNow = LocalDateTime.now();
            DateTimeFormatter myFormatDayTime = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
            String MyDayTimeNow = dateTimeNow.format(myFormatDayTime);
            System.out.println("Now date and time: " + MyDayTimeNow);
        }

    }
}
